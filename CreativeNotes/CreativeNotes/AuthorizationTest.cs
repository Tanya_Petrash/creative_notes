using CreativeNotes.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace CreativeNotes
{
    public class Tests
    {
        IWebDriver driver;
        [SetUp]
        public void Setup()
        {
            
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://3.133.100.46/login");
            driver.Manage().Window.Maximize(); 
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [TestCase("Ivan", "qwert12345", "http://3.133.100.46/")]
        public void CheckAuthorizationWithValidData(string login, string password, string ExpURL)
        {
            AuthorizationPageObgect authorizationPageObgect = new AuthorizationPageObgect(driver);
            authorizationPageObgect.Authorization(login, password);
            Assert.AreEqual(ExpURL,authorizationPageObgect.GetUrl(), "Sorry...Wrong information");
        }
        [TestCase("", "qwert12345", "The login should only consist of letters and numbers from 4 to 12 characters long")]
        public void CheckAuthorizationWithNotValidLogin(string login, string password, string ExpError)
        {
            AuthorizationPageObgect authorizationPageObgect = new AuthorizationPageObgect(driver);
            authorizationPageObgect.Authorization(login, password);
            Thread.Sleep(3000);
            var actualError = authorizationPageObgect.GetNotValidEmailError();
            Assert.AreEqual(ExpError, actualError, "Sorry...Wrong information");
        }
    }
}
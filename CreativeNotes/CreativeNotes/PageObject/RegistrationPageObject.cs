﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNotes.PageObject
{
    class RegistrationPageObject
    {
        private IWebDriver webdriver;
        private readonly By _logInInputButton = By.XPath("//input[@type='text']");
        private readonly By _passwordInputButton = By.XPath("//input[@placeholder='Enter your password...']");
        private readonly By _passwordConfirmInputButton = By.XPath("//input[@placeholder='Confirm password...']");
        private readonly By _signUpButton = By.XPath("//button[@class='sc-pGacB kIbOXm']");
        public string GetUrl() => webdriver.Url;
        IWebElement GetLoginField() => webdriver.FindElement(_logInInputButton);
        IWebElement GetPasswordField() => webdriver.FindElement(_passwordInputButton);
        IWebElement GetPasswordConfirmField() => webdriver.FindElement(_passwordConfirmInputButton);
        IWebElement GetSignUpButton() => webdriver.FindElement(_signUpButton);



        public RegistrationPageObject(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        public string GetNotValidEmailError() => webdriver.FindElement(By.CssSelector("//*[@id='v1zslylwp']/div[1]/div[2]/text()")).Text;
        public string GetNotValidPasswordError() => webdriver.FindElement(By.CssSelector("//*[@id='8da61nawg']/div[1]/div[2]/text()")).Text;

        public string GetNotValidPasswordConfirmError() => webdriver.FindElement(By.CssSelector("//*[@id='0jnk748uq']/div[1]/div[2]")).Text;
        public AuthorizationPageObgect Registration(string login,string passsword,string passwordconfirm)
        {
            var loginField = GetLoginField();
            loginField.SendKeys(login);
            var passwordField = GetPasswordField();
            passwordField.SendKeys(passsword);
            var passwordConfirmField = GetPasswordConfirmField();
            passwordConfirmField.SendKeys(passwordconfirm);
            var signup = GetSignUpButton();
            signup.Click();
            return new AuthorizationPageObgect(webdriver);
        }
    }
}

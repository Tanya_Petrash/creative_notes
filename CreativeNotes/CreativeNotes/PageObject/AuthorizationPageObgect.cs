﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNotes.PageObject
{
    class AuthorizationPageObgect
    {
        private IWebDriver webdriver;
        private readonly By _logInInputButton = By.XPath("//input[@type='text']");
        private readonly By _passwordInputButton = By.XPath("//input[@type='password']");
        private readonly By _LogInButton = By.XPath("//button[@class='sc-pGacB kIbOXm']");
        public string GetUrl() => webdriver.Url;
        IWebElement GetLoginField() => webdriver.FindElement(_logInInputButton);
        IWebElement GetPasswordField() => webdriver.FindElement(_passwordInputButton);
        IWebElement GetLoginButton() => webdriver.FindElement(_LogInButton);
        public string GetNotValidEmailError() => webdriver.FindElement(By.CssSelector("div[role='alert'] > div:nth-child(2)")).Text;




        public AuthorizationPageObgect(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        public MainMenuPageObject Authorization(string login, string password)
        {
            var loginField = GetLoginField();
            loginField.SendKeys(login);
            var passwordField = GetPasswordField();
            passwordField.SendKeys(password);
            var loginButton = GetLoginButton();
            loginButton.Click();
            return new MainMenuPageObject(webdriver);
        }
    }
}

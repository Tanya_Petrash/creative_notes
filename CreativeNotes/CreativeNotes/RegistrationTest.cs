﻿using CreativeNotes.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace CreativeNotes
{
    class RegistrationTest
    {
        IWebDriver driver;
        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://3.133.100.46/registration");
            driver.Manage().Window.Maximize(); 
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        [TestCase("Senya", "qwert12345", "qwert12345", "http://3.133.100.46/")]
        public void CheckRegistrationWithValidData(string login, string password,string passwordconfirm ,string ExpURL)
        {
            RegistrationPageObject registrationPageObject = new RegistrationPageObject(driver);
            AuthorizationPageObgect authorizationPageObgect = new AuthorizationPageObgect(driver);
            registrationPageObject.Registration(login, password, passwordconfirm);
            Thread.Sleep(3000);
            authorizationPageObgect.Authorization(login, password);
            Assert.AreEqual(ExpURL, authorizationPageObgect.GetUrl(), "Sorry...Wrong information");
        }
        [TestCase("", "qwert12345", "qwert12345", "The login should only consist of letters and numbers from 4 to 12 characters long", "http://3.133.100.46/login")]
        [TestCase("nat", "qwert12345", "qwert12345", "The login should only consist of letters and numbers from 4 to 12 characters long", "http://3.133.100.46/login")]
        [TestCase("nata", "qwert12345", "qwert12345", "The login should only consist of letters and numbers from 4 to 12 characters long", "http://3.133.100.46/login")]
        [TestCase("nata1", "qwert12345", "qwert12345", "The login should only consist of letters and numbers from 4 to 12 characters long", "http://3.133.100.46/login")]
        [TestCase("+++++", "qwert12345", "qwert12345", "The login should only consist of letters and numbers from 4 to 12 characters long", "http://3.133.100.46/login")]
        [TestCase("nata123456789", "qwert12345", "qwert12345", "The login should only consist of letters and numbers from 4 to 12 characters long", "http://3.133.100.46/login")]

        public void CheckRegistrationWithNotValidLogin(string login, string password, string passwordconfirm, string ExpError, string ExpURL)
        {
            RegistrationPageObject registrationPageObject = new RegistrationPageObject(driver);
            registrationPageObject.Registration(login, password, passwordconfirm);
            var actualError = registrationPageObject.GetNotValidEmailError();
            Assert.AreEqual(ExpError, actualError, "Sorry...Wrong information");
            Assert.AreEqual(ExpURL, registrationPageObject.GetUrl(), "Sorry...Wrong information");
        }
        [TestCase("Nata", "12345", "12345", "The password must be from 4 to 12 characters long, consist only of letters and numbers, be sure to contain 1 letter and 1 number","http://3.133.100.46/login")]
        [TestCase("Nata", "nata", "nata", "The password must be from 4 to 12 characters long, consist only of letters and numbers, be sure to contain 1 letter and 1 number", "http://3.133.100.46/login")]
        [TestCase("Nata", "n1", "n1", "The password must be from 4 to 12 characters long, consist only of letters and numbers, be sure to contain 1 letter and 1 number", "http://3.133.100.46/login")]
        public void CheckRegistrationWithNotValidPassword(string login, string password, string passwordconfirm, string ExpError, string ExpURL)
        {
            RegistrationPageObject registrationPageObject = new RegistrationPageObject(driver);
            registrationPageObject.Registration(login, password, passwordconfirm);
            var actualError = registrationPageObject.GetNotValidPasswordError();
            Assert.AreEqual(ExpError, actualError, "Sorry...Wrong information");
            Assert.AreEqual(ExpURL, registrationPageObject.GetUrl(), "Sorry...Wrong information");
        }
        [TestCase("Nata", "12345nata","nata12345", "Password mismatch", "http://3.133.100.46/login")]
        public void CheckRegistrationWithNotValidPasswordConfirm(string login, string password, string passwordconfirm, string ExpError, string ExpURL)
        {
            RegistrationPageObject registrationPageObject = new RegistrationPageObject(driver);
            registrationPageObject.Registration(login, password, passwordconfirm);
            var actualError = registrationPageObject.GetNotValidPasswordConfirmError();
            Assert.AreEqual(ExpError, actualError, "Sorry...Wrong information");
            Assert.AreEqual(ExpURL, registrationPageObject.GetUrl(), "Sorry...Wrong information");
        }
    }
}
